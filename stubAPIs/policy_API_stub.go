package stubAPIs

import (
	"encoding/json"
	"main/utilities"
	"net/http"
	"path"
	"strings"
)

// Policies_api is a mock API of the policies API for testing purposes.

// HandlerPolicy is the entry point for the mock API.
func HandlerPolicy(w http.ResponseWriter, r *http.Request) {
	// Gets the country code from the URL.
	cleanPath := path.Clean(r.URL.Path)
	pathList := strings.Split(cleanPath, "/")
	country := pathList[1]
	var response utilities.PolicyActions
	// Checks which country code was requested, and returns the correct response.
	if country == "NOS" {
		response = utilities.PolicyActions{
			Policies: []utilities.PolicyStruct{
				{Policy_type_code: "NONE"},
			},
			Stringency: utilities.StringencyData(struct {
				DateValue        string
				CountryCode      string
				Confirmed        int
				Deaths           int
				StringencyActual float64
				Stringency       float64
			}{DateValue: "", CountryCode: "", Confirmed: 0, Deaths: 0, StringencyActual: 0.0, Stringency: 0.0}),
		}
	} else if country == "SWE" {
		response = utilities.PolicyActions{
			Policies: []utilities.PolicyStruct{
				{Policy_type_code: "NONE"},
			},
			Stringency: utilities.StringencyData(struct {
				DateValue        string
				CountryCode      string
				Confirmed        int
				Deaths           int
				StringencyActual float64
				Stringency       float64
			}{DateValue: "2022-03-26", CountryCode: country, Confirmed: 1396911, Deaths: 2339, StringencyActual: 12.69, Stringency: 13.89}),
		}
	} else if country == "FRA" {
		response = utilities.PolicyActions{
			Policies: []utilities.PolicyStruct{
				{Policy_type_code: "NONE"},
			},
			Stringency: utilities.StringencyData(struct {
				DateValue        string
				CountryCode      string
				Confirmed        int
				Deaths           int
				StringencyActual float64
				Stringency       float64
			}{DateValue: "2022-03-26", CountryCode: country, Confirmed: 1396911, Deaths: 2339, StringencyActual: 0.0, Stringency: 0.0}),
		}
	} else {
		response = utilities.PolicyActions{
			Policies: []utilities.PolicyStruct{
				{Policy_type_code: "NONE"},
			},
			Stringency: utilities.StringencyData(struct {
				DateValue        string
				CountryCode      string
				Confirmed        int
				Deaths           int
				StringencyActual float64
				Stringency       float64
			}{DateValue: "2022-03-26", CountryCode: country, Confirmed: 1396911, Deaths: 2339, StringencyActual: 0.0, Stringency: 13.89}),
		}
	}
	// Encode the response to JSON, and write it to the response.
	encoder := json.NewEncoder(w)
	// pretty printing
	encoder.SetIndent("", "\t")
	// encodes the response
	err := encoder.Encode(response)
	if err != nil {
		panic(err)
	}
}
