package stubAPIs

// country_api is a mock API of the country API for testing purposes.

import (
	"encoding/json"
	"main/utilities"
	"net/http"
	"path"
)

// HandlerCountries is the entry point for the mock API.
func HandlerCountries(w http.ResponseWriter, r *http.Request) {
	// Get the country name from the URL.
	cleanPath := path.Clean(r.URL.Path)
	country := path.Base(cleanPath)

	var response utilities.Country
	// Checks which country is requested, and returns the correct response.
	if country == "NOR" {
		response = utilities.Country{Names: utilities.Name{Common: "Norway"}}
	} else if country == "SWE" {
		response = utilities.Country{Names: utilities.Name{Common: "Sweden"}}
	} else if country == "DNK" {
		response = utilities.Country{Names: utilities.Name{Common: "Denmark"}}
	}

	// Encode the response to JSON, and write it to the response.
	encoder := json.NewEncoder(w)
	// pretty printing
	encoder.SetIndent("", "\t")
	// encodes the response
	err := encoder.Encode(response)
	if err != nil {
		panic(err)
	}
}
