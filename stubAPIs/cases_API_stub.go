package stubAPIs

// cases_api is a mock API of the cases API for testing purposes.

import (
	"encoding/json"
	"fmt"
	"main/utilities"
	"net/http"
	"strings"
)

type queryStruct struct {
	Query string `json:"query"`
}

// HandlerCases is the entry point of the mock api.
func HandlerCases(w http.ResponseWriter, r *http.Request) {

	// Get the query from the request.
	var query queryStruct
	err := json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		fmt.Println(err.Error())
	}

	var response utilities.DataStruct

	// Checks if the query contains "Taiwan", and returns an empty struct.
	if strings.Contains(query.Query, "Taiwan") {
		response = utilities.DataStruct{
			Data: utilities.CountryStruct{
				Country: utilities.CovidCases{
					CountryName: "",
					MostRecent: utilities.MostRecent{
						Date:       "",
						Confirmed:  0,
						Deaths:     0,
						Recovered:  0,
						GrowthRate: 0,
					},
				},
			},
		}

	} else {
		// Responds with a standard response.
		response = utilities.DataStruct{
			Data: utilities.CountryStruct{
				Country: utilities.CovidCases{
					CountryName: "Norway",
					MostRecent: utilities.MostRecent{
						Date:       "2020-01-01",
						Confirmed:  1,
						Recovered:  2,
						Deaths:     3,
						GrowthRate: 4,
					},
				},
			},
		}

	}

	encoder := json.NewEncoder(w)
	// pretty printing
	encoder.SetIndent("", "\t")
	// encodes the response
	err = encoder.Encode(response)
	if err != nil {
		panic(err)
	}

}
