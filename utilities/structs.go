package utilities

type PrettyDataStruct struct {
	Country    string  `json:"country"`
	Date       string  `json:"date"`
	Confirmed  int     `json:"confirmed"`
	Recovered  int     `json:"recovered"`
	Deaths     int     `json:"deaths"`
	GrowthRate float64 `json:"growth_rate"`
}

type DataStruct struct {
	Data CountryStruct `json:"data"`
}

type CountryStruct struct {
	Country CovidCases `json:"country"`
}

type CovidCases struct {
	CountryName string     `json:"name"`
	MostRecent  MostRecent `json:"MostRecent"`
}

type MostRecent struct {
	Date       string  `json:"date"`
	Confirmed  int     `json:"confirmed"`
	Deaths     int     `json:"deaths"`
	Recovered  int     `json:"recovered"`
	GrowthRate float64 `json:"growthRate"`
}

type PolicyActions struct {
	Policies   []PolicyStruct `json:"policyActions"`
	Stringency StringencyData `json:"stringencyData"`
}

type PolicyStruct struct {
	Policy_type_code string `json:"policy_type_code"`
}

type StringencyData struct {
	DateValue        string  `json:"date_value"`
	CountryCode      string  `json:"country_code"`
	Confirmed        int     `json:"confirmed"`
	Deaths           int     `json:"deaths"`
	StringencyActual float64 `json:"stringency_actual"`
	Stringency       float64 `json:"stringency"`
}

type Response struct {
	CountryCode string  `json:"country_code"`
	Scope       string  `json:"scope"`
	Stringency  float64 `json:"stringency"`
	Policies    int     `json:"policies"`
}

type DiagStruct struct {
	CasesApi     string `json:"cases_api"`
	PolicyApi    string `json:"policy_api"`
	CountriesApi string `json:"countries_api"`
	Webhooks     int    `json:"webhooks"`
	Version      string `json:"version"`
	Uptime       string `json:"uptime"`
}

type Country struct {
	Names Name `json:"name"`
}

type Name struct {
	Common string `json:"common"`
}

type Webhook struct {
	Url         string `json:"Url"`
	Country     string `json:"Country"`
	Calls       int    `json:"Calls"`
	ActualCalls int    `json:"Actual_calls"`
}

type WebHookStruct struct {
	WebhookId string `json:"webhook_id"`
	Url       string `json:"url"`
	Country   string `json:"country"`
	Calls     int    `json:"calls"`
}

type WebHookStruct2 struct {
	WebhookId string `json:"webhook_id"`
	Country   string `json:"country"`
	Calls     int    `json:"calls"`
}
