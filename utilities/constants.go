package utilities

var (
	CasesApi     = "https://covid19-graphql.vercel.app/"
	PolicyApi    = "https://covidtrackerapi.bsg.ox.ac.uk/api/"
	CountriesApi = "http://restcountries.com/"
)

const VERSION = "v1"
const CORONA = "/corona/"

var (
	WebhooksCollection = "db"
	CasesCollection    = "casesCache"
	PolicyCollection   = "policyCache"
)

const IndexPath = "/"
const CasesPath = CORONA + VERSION + "/cases/"
const PolicyPath = CORONA + VERSION + "/policy/"
const StatusPath = CORONA + VERSION + "/status/"

const NotificationPath = CORONA + VERSION + "/notifications/"

var PolicyActionsEndpoint = "v2/stringency/actions/"

var CountriesAlphaEndpoint = "v3.1/alpha/"

func SetTestUrls(casesUrl string, policyUrl string, countriesUrl string) {
	CasesApi = casesUrl + "/"
	PolicyApi = policyUrl + "/"
	CountriesApi = countriesUrl + "/"
	PolicyActionsEndpoint = ""
	CountriesAlphaEndpoint = ""
}

func SetTestCollection() {
	WebhooksCollection = "db_test"
	CasesCollection = "casesCache_test"
	PolicyCollection = "policyCache_test"
}
