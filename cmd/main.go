package main

import (
	"log"
	"main/db"
	"main/handlers/cases_handlers"
	"main/handlers/indexHandlers"
	"main/handlers/notificationHandlers"
	"main/handlers/policyHandlers"
	"main/handlers/statusHandlers"
	"main/utilities"
	"net/http"
	"os"
)

func main() {
	go utilities.ReturnStartTime()
	db.InitDB("../assignment-2-17921-firebase-adminsdk-ekmn8-70f341d0e2.json")

	// Extract PORT variable from the environment variables - used in Heroku
	port := os.Getenv("PORT")

	// Override port with default port if not provided (e.g. local deployment)
	if port == "" {
		log.Println("$PORT has not been set. Default: 8080")
		port = "8080"
	}

	http.HandleFunc(utilities.CasesPath, cases_handlers.CaseHandler)

	http.HandleFunc(utilities.PolicyPath, policyHandlers.PolicyHandler)

	http.HandleFunc(utilities.StatusPath, statusHandlers.DiagHandler)

	http.HandleFunc(utilities.NotificationPath, notificationHandlers.NotificationHandler)

	http.HandleFunc(utilities.IndexPath, indexHandlers.IndexHandler)

	// Start HTTP server
	log.Println("Starting server on port " + port + " ...")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
