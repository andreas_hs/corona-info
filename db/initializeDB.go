package db

import (
	"cloud.google.com/go/firestore" // Firestore-specific support
	"context"
	firebase "firebase.google.com/go" // Generic firebase support
	"google.golang.org/api/option"
	"log"
)

var Ctx context.Context
var Client *firestore.Client

func InitDB(filename string) {
	// Firebase initialisation
	Ctx = context.Background()

	// We use a service account, load credentials file that you downloaded from your project's settings menu.
	// It should reside in your project directory.
	// Make sure this file is git-ignored, since it is the access token to the database.
	sa := option.WithCredentialsFile(filename)

	app, err := firebase.NewApp(Ctx, nil, sa)
	if err != nil {
		log.Fatalln(err)
	}

	// Instantiate client
	Client, err = app.Firestore(Ctx)

	if err != nil {
		log.Fatalln(err)
	}

}
