FROM golang:1.17

LABEL maintainer="andrsell@ntnu.no"


WORKDIR /go/src/app/cmd

COPY ./cmd /go/src/app/cmd
COPY ./db /go/src/app/db
COPY ./handlers /go/src/app/handlers
COPY ./utilities /go/src/app/utilities
COPY ./go.mod /go/src/app/go.mod
COPY ./go.sum /go/src/app/go.sum
COPY ./assignment-2-17921-firebase-adminsdk-ekmn8-70f341d0e2.json /go/src/app/assignment-2-17921-firebase-adminsdk-ekmn8-70f341d0e2.json

RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o server

CMD ["./server"]
