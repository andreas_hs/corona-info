package statusHandlers

import (
	"encoding/json"
	"log"
	"main/db"
	"main/utilities"
	"net/http"
	"sync"
)

var casesReply string
var policyReply string
var countryReply string

func DiagHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		// gets the statuscodes for the different APIs
		assignStatusCodeStrings()
		//replies with the status codes and the uptime
		ReplyWithStatus(w, CombineISapiUpData(casesReply, policyReply, countryReply, utilities.TimeSinceStart()))
		break
	default:
		http.Error(w, "method not supported", http.StatusMethodNotAllowed)
	}
}

func assignStatusCodeStrings() {

	// makes a new wait group
	wg := new(sync.WaitGroup)
	// adds 3 to the counter
	wg.Add(3)

	// makes three threads
	go assignCasesStrings(wg)
	go assignPolicyStrings(wg)
	go assignCountryStrings(wg)

	// waits for the counter to be zero
	wg.Wait()
}

// assigns casesString
func assignCasesStrings(wg *sync.WaitGroup) string {
	// gets the status code from the api
	casesReply = IsCasesAPIUp()
	// removes one from the wg counter
	defer wg.Done()
	// returns the status code
	return casesReply
}

//assigns policyString
func assignPolicyStrings(wg *sync.WaitGroup) string {
	// gets the status code from the api
	policyReply = IsPolicyAPIUp()
	// removes one from the wg counter
	defer wg.Done()
	// returns the status code
	return policyReply
}

//assigns countryString
func assignCountryStrings(wg *sync.WaitGroup) string {
	// gets the status code from the api
	countryReply = IsCountryAPIup()
	// removes one from the wg counter
	defer wg.Done()
	// returns the status code
	return countryReply
}

// GetNumberOfWebhooks gets the number of webhooks
func GetNumberOfWebhooks() int {
	getDocuments, err := db.Client.Collection(utilities.WebhooksCollection).Documents(db.Ctx).GetAll()
	// Error handling
	if err != nil {
		log.Print("No documents found in the specified collection" + err.Error())
		return 0
	}
	numberOfWebhooks := len(getDocuments)
	return numberOfWebhooks
}

// CombineISapiUpData combines the data from the different methods to a new struct
func CombineISapiUpData(casesReply string, policyReply string, countryReply string, time float64) utilities.DiagStruct {

	combinedResponse := utilities.DiagStruct{
		CasesApi:     casesReply,
		PolicyApi:    policyReply,
		CountriesApi: countryReply,
		Webhooks:     GetNumberOfWebhooks(),
		Version:      utilities.VERSION,
		Uptime:       utilities.SecondsToHuman(int(time)),
	}
	return combinedResponse
}

//  gets the status code from the cases api
func IsCasesAPIUp() string {
	resp, err := http.Get(utilities.CasesApi + "?query=%7B__typename%7D")

	if err != nil {
		return "502 Bad Gateway"
	}
	return resp.Status
}

// gets the status code from the policy api
func IsPolicyAPIUp() string {
	resp, err := http.Get(utilities.PolicyApi)
	if err != nil {
		return "502 Bad Gateway"
	}
	return resp.Status
}

//gets the status code from the countries api
func IsCountryAPIup() string {
	resp, err := http.Get(utilities.CountriesApi)
	if err != nil {
		return "502 Bad Gateway"
	}
	return resp.Status
}

//replies with the combined data
func ReplyWithStatus(w http.ResponseWriter, diagStruct utilities.DiagStruct) {
	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err := encoder.Encode(diagStruct)
	if err != nil {
		http.Error(w, "ERROR encoding", http.StatusInternalServerError)
	}
}
