package statusHandlers

import (
	"encoding/json"
	"main/db"
	"main/stubAPIs"
	"main/utilities"
	"net/http"
	"net/http/httptest"
	"testing"
)

var statusEndpoint *httptest.Server

func TestMain(m *testing.M) {
	policyMock := httptest.NewServer(http.HandlerFunc(stubAPIs.HandlerPolicy))
	defer policyMock.Close()
	casesMock := httptest.NewServer(http.HandlerFunc(stubAPIs.HandlerCases))
	defer casesMock.Close()
	countriesMock := httptest.NewServer(http.HandlerFunc(stubAPIs.HandlerCountries))
	defer countriesMock.Close()

	utilities.SetTestUrls(casesMock.URL, policyMock.URL, countriesMock.URL)
	utilities.SetTestCollection()

	db.InitDB("../../assignment-2-17921-firebase-adminsdk-ekmn8-70f341d0e2.json")

	statusEndpoint = httptest.NewServer(http.HandlerFunc(DiagHandler))
	defer statusEndpoint.Close()

	utilities.ReturnStartTime()

	m.Run()
}

func TestRequestToStatus(t *testing.T) {
	webhooks := GetNumberOfWebhooks()
	type args struct {
		url string
	}
	tests := []struct {
		name               string
		args               args
		method             string
		expectedStatusCode int
		expectedResponse   utilities.DiagStruct
	}{
		{
			name: "Valid Get Request to Status Endpoint",
			args: args{
				url: statusEndpoint.URL + "/corona/v1/status",
			},
			expectedStatusCode: http.StatusOK,
			expectedResponse: utilities.DiagStruct{
				CasesApi:     "200 OK",
				PolicyApi:    "200 OK",
				CountriesApi: "200 OK",
				Webhooks:     webhooks,
				Version:      "v1",
				Uptime:       utilities.SecondsToHuman(int(utilities.TimeSinceStart())),
			},
		},
		{
			name: "Valid Post Request to Status Endpoint",
			args: args{
				url: statusEndpoint.URL + "/corona/v1/status",
			},
			method:             http.MethodPost,
			expectedStatusCode: http.StatusMethodNotAllowed,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client := &http.Client{}
			req, err := http.NewRequest(tt.method, tt.args.url, nil)
			if err != nil {
				t.Errorf("Error creating request: %v", err)
			}
			res, err := client.Do(req)
			if err != nil {
				t.Errorf("Error making %s request: %s", tt.method, err)
			}
			if res.StatusCode != tt.expectedStatusCode {
				t.Errorf("Expected %d response, got %d", tt.expectedStatusCode, res.StatusCode)
			}

			if tt.expectedResponse != (utilities.DiagStruct{}) {
				tt.expectedResponse.Uptime = utilities.SecondsToHuman(int(utilities.TimeSinceStart()))
				var actual utilities.DiagStruct
				_ = json.NewDecoder(res.Body).Decode(&actual)

				if actual != tt.expectedResponse {
					t.Errorf("Expected %v, got %v", tt.expectedResponse, actual)
				}
			}
		})
	}
}
