package indexHandlers

import (
	"main/utilities"
	"net/http"
	"strings"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
	case http.MethodGet:
		indexGetRequest(w, r)
	default:
		http.Error(w, "method not supported.", http.StatusNotImplemented)
		return
	}

}

// makes strings with info about the endpoints, and how to use them.
func indexGetRequest(w http.ResponseWriter, r *http.Request) {
	var text strings.Builder

	text.WriteString("there is no functionality on this level, please use one of the given endpoints:" + "\n")
	text.WriteString(utilities.CasesPath + "\n")
	text.WriteString(utilities.PolicyPath + "\n")
	text.WriteString(utilities.StatusPath + "\n")
	text.WriteString(utilities.NotificationPath + "\n")
	text.WriteString("for usage see readme" + "\n")

	http.Error(w, text.String(), http.StatusNotFound)
}
