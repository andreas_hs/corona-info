package cases_handlers

import (
	"bytes"
	"encoding/json"
	"google.golang.org/api/iterator"
	"io"
	"io/ioutil"
	"main/db"
	"main/handlers/notificationHandlers"
	"main/utilities"
	"net/http"
	"strings"
	"time"
)

func CaseHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		handleGetRequest(w, r)
	default:
		http.Error(w, "method not supported", 405)
	}
}

func handleGetRequest(w http.ResponseWriter, r *http.Request) {

	// spilts the string on every /
	parts := strings.Split(r.URL.Path, "/")

	if len(parts) > 5 {
		http.Error(w, "Too many arguments", http.StatusBadRequest) // status bad request
		return
	}

	// inits countryName
	countryName := ""

	// checks if the length of parts[4] is equals 2, if so it is handeled as a shortname e.g. US
	if len(parts[4]) == 2 {
		countryName = strings.ToUpper(parts[4])
	} else if len(parts[4]) == 3 { //checks if the length of parts[4] is equals 3, if so it is a alpha 3 code

		// gets the country name from the country api
		countryName = FindCountryUsingAlpha(w, strings.ToUpper(parts[4]))
	} else {
		//normal country name
		countryName = strings.Title(strings.ToLower(parts[4]))
	}
	go notificationHandlers.CountUses(w, countryName)
	replyResponse(createResponse(w, countryName), w)
}

// this method finds the common name of a country using the alpha 3 code
func FindCountryUsingAlpha(w http.ResponseWriter, alphaCode string) string {

	// builds the string that is sent to the get request
	var stringToSend strings.Builder
	stringToSend.WriteString(utilities.CountriesApi + utilities.CountriesAlphaEndpoint)
	stringToSend.WriteString(alphaCode)
	stringToSend.WriteString("?fields=name")

	//gets the information from the api
	resp, err := http.Get(stringToSend.String())

	//error handling
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return ""
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error.", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return ""
	}

	var response utilities.Country
	err = json.Unmarshal(body, &response)

	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return ""
	}
	// returns the name from the response
	return response.Names.Common
}

func createResponse(w http.ResponseWriter, country string) utilities.PrettyDataStruct {
	if country == "" {
		http.Error(w, "No country name given", http.StatusBadRequest)
		return utilities.PrettyDataStruct{}
	}

	cacheResp := checkCasesCache(country)
	if (cacheResp != utilities.PrettyDataStruct{}) {
		if cacheResp.Date == time.Now().Add(-24*time.Hour).Format("2006-01-02") {
			return cacheResp
		} else {
			deleteCache(w, country)
		}
	}

	jsonData := map[string]string{
		"query": `query {
country(name: "` + country + `"){
name
mostRecent{
date(format: "yyyy-MM-dd")
confirmed
recovered
deaths
growthRate
		}
	}
}`,
	}

	// error handling
	marshaledData, err := json.Marshal(jsonData)
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)

		return utilities.PrettyDataStruct{}

	}
	newRequest, err := http.NewRequest(http.MethodPost, utilities.CasesApi, bytes.NewBuffer(marshaledData))
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return utilities.PrettyDataStruct{}
	}

	webClient := &http.Client{}
	newRequest.Header.Add("content-type", "application/json")

	response, _ := webClient.Do(newRequest)
	decoder := json.NewDecoder(response.Body)
	structTing := utilities.DataStruct{}

	err = decoder.Decode(&structTing)

	// error handling
	if err != nil {
		http.Error(w, "Internal server error.", http.StatusInternalServerError)
		return utilities.PrettyDataStruct{}
	}
	newResp := MakeStructPretty(structTing)
	//adds the new resp to the cache
	if newResp.Country != "" {
		id, _, err := db.Client.Collection(utilities.CasesCollection).Add(db.Ctx, newResp)

		if err != nil {
			// Error handling
			http.Error(w, "Error when adding "+id.ID+", Error: "+err.Error(), http.StatusBadRequest)
		}
	}
	return newResp
}

func deleteCache(w http.ResponseWriter, country string) {
	iter := db.Client.Collection(utilities.CasesCollection).Where("Country", "==", country).Documents(db.Ctx)
	for {

		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			break
		}
		_, err2 := db.Client.Collection(utilities.CasesCollection).Doc(doc.Ref.ID).Delete(db.Ctx)
		if err2 != nil {
			http.Error(w, "error while deleting", http.StatusInternalServerError)
			break
		}
	}
}

func MakeStructPretty(structTing utilities.DataStruct) utilities.PrettyDataStruct {
	t := utilities.PrettyDataStruct{
		Country:    structTing.Data.Country.CountryName,
		Date:       structTing.Data.Country.MostRecent.Date,
		Confirmed:  structTing.Data.Country.MostRecent.Confirmed,
		Recovered:  structTing.Data.Country.MostRecent.Recovered,
		Deaths:     structTing.Data.Country.MostRecent.Deaths,
		GrowthRate: structTing.Data.Country.MostRecent.GrowthRate,
	}
	return t
}

func replyResponse(response utilities.PrettyDataStruct, w http.ResponseWriter) {
	// error handling
	if response == (utilities.PrettyDataStruct{}) {
		http.Error(w, "no data found", http.StatusBadRequest)
		return
	}
	// makes a new encoder
	encoder := json.NewEncoder(w)
	// pretty printing
	encoder.SetIndent("", "\t")
	// encodes the response
	err := encoder.Encode(response)
	if err != nil {
		http.Error(w, "ERROR encoding.", http.StatusInternalServerError)
		return
	}

}

func checkCasesCache(countryName string) utilities.PrettyDataStruct {
	iter := db.Client.Collection(utilities.CasesCollection).Where("Country", "==", countryName).Documents(db.Ctx)
	var response utilities.PrettyDataStruct
	for {

		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			break
		}
		response = utilities.PrettyDataStruct{
			Country:    doc.Data()["Country"].(string),
			Date:       doc.Data()["Date"].(string),
			Confirmed:  int(doc.Data()["Confirmed"].(int64)),
			Recovered:  int(doc.Data()["Recovered"].(int64)),
			Deaths:     int(doc.Data()["Deaths"].(int64)),
			GrowthRate: doc.Data()["GrowthRate"].(float64),
		}

		return response
	}
	return utilities.PrettyDataStruct{}
}
