package notificationHandlers

import (
	"bytes"
	"cloud.google.com/go/firestore" // Firestore-specific support
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"main/db"
	"main/utilities"
	"net/http"
	"strings"
)

func NotificationHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Method)
	switch r.Method {
	case http.MethodGet:
		handleNotificationGetRequest(w, r)
		break
	case http.MethodPost:
		handleNotificationPostRequest(w, r)
		break
	case http.MethodDelete:
		handleNotificationDeleteRequest(w, r)
		break
	default:
		http.Error(w, "method not supported", 501)
	}
}

func handleNotificationDeleteRequest(w http.ResponseWriter, r *http.Request) {
	db.InitDB("../assignment-2-17921-firebase-adminsdk-ekmn8-70f341d0e2.json")
	parts := strings.Split(r.URL.Path, "/")
	if (len(parts)) != 5 || parts[4] == "" {
		http.Error(w, "path is of invalid length", http.StatusBadRequest)
		return
	}
	id := parts[4]

	doc, _ := db.Client.Collection(utilities.CasesCollection).Doc(id).Get(db.Ctx)
	if !doc.Exists() {
		http.Error(w, "Document does not exist", http.StatusBadRequest)
		return
	}
	_, err := db.Client.Collection(utilities.CasesCollection).Doc(id).Delete(db.Ctx)
	if err != nil {
		http.Error(w, "error while deleting", http.StatusInternalServerError)
		return
	}
	http.Error(w, "successfully deleted", http.StatusOK)
}

func handleNotificationGetRequest(w http.ResponseWriter, r *http.Request) {
	replyWebhookData(w, getWebHook(w, r))
}

func getWebHook(w http.ResponseWriter, r *http.Request) interface{} {
	parts := strings.Split(r.URL.Path, "/")
	if (len(parts)) != 5 || parts[4] == "" {
		return GetAll()
	}
	id := parts[4]

	doc, _ := db.Client.Collection(utilities.WebhooksCollection).Doc(id).Get(db.Ctx)
	if !doc.Exists() {
		http.Error(w, "document does not exist", http.StatusNotFound)
		return utilities.WebHookStruct{}
	}
	document := db.Client.Collection(utilities.CasesCollection).Doc(id)
	docsnap, _ := document.Get(db.Ctx)
	var webhook utilities.WebHookStruct
	if err := docsnap.DataTo(&webhook); err != nil {
		http.Error(w, "error decoding", http.StatusInternalServerError)
	}
	webhookResponse := utilities.WebHookStruct{
		WebhookId: id,
		Url:       webhook.Url,
		Country:   webhook.Country,
		Calls:     webhook.Calls,
	}
	return webhookResponse
}

func GetAll() []utilities.WebHookStruct {
	var bs []utilities.WebHookStruct
	iter := db.Client.Collection(utilities.CasesCollection).Documents(db.Ctx)
	for {
		var b utilities.Webhook
		doc, err := iter.Next()
		if err != nil {
			fmt.Println(err)
		}
		if err == iterator.Done {
			break
		}
		if err := doc.DataTo(&b); err != nil {
			fmt.Println(doc.Data())

		}
		webhookResponse := utilities.WebHookStruct{
			WebhookId: doc.Ref.ID,
			Url:       b.Url,
			Country:   b.Country,
			Calls:     b.Calls,
		}
		bs = append(bs, webhookResponse)
	}
	fmt.Println(len(bs))
	return bs
}

func handleNotificationPostRequest(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var t utilities.Webhook
	err := decoder.Decode(&t)
	t.Country = strings.Title(strings.ToLower(t.Country))
	if err != nil {
		http.Error(w, "error decoding", http.StatusInternalServerError)
		return
	}
	fmt.Println(t.Country)

	if len(t.Country) == 0 {
		http.Error(w, "Your message appears to be empty. Ensure to terminate URI with /.", http.StatusBadRequest)
	} else {
		id, _, err := db.Client.Collection(utilities.CasesCollection).Add(db.Ctx, t)

		if err != nil {
			// Error handling
			http.Error(w, "Error when adding message "+t.Country+", Error: "+err.Error(), http.StatusBadRequest)
			return
		} else {
			fmt.Println("Entry added to collection. Identifier of returned document: " + id.ID)
			// Returns document ID in body

			http.Error(w, "webhook_id: "+id.ID, http.StatusCreated)
		}
		return
	}
}

func replyWebhookData(w http.ResponseWriter, structToSend interface{}) {
	if (structToSend == utilities.WebHookStruct{}) {
		return
	}

	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err := encoder.Encode(structToSend)
	if err != nil {
		http.Error(w, "ERROR encoding.", http.StatusInternalServerError)
		return
	}
}

func CountUses(w http.ResponseWriter, countryName string) {
	iter := db.Client.Collection(utilities.CasesCollection).Where("Country", "==", countryName).Documents(db.Ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			break
		}

		_, err = db.Client.Collection(utilities.CasesCollection).Doc(doc.Ref.ID).Update(db.Ctx, []firestore.Update{
			{
				Path:  "ActualCalls",
				Value: firestore.Increment(1), //doc.Data()["ActualCalls"].(int64) + 1
			},
		})
		if err != nil {
			break
		}
	}
	go invokeWebhook(w, countryName)

}

func invokeWebhook(w http.ResponseWriter, countryName string) {
	iter := db.Client.Collection(utilities.CasesCollection).Where("Country", "==", countryName).Documents(db.Ctx)

	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			break
		}
		//oga boga code
		if doc.Data()["ActualCalls"] == nil || doc.Data()["Calls"] == nil {
			break
		}
		if doc.Data()["ActualCalls"].(int64) >= doc.Data()["Calls"].(int64) {
			//reset counter
			_, err = db.Client.Collection(utilities.CasesCollection).Doc(doc.Ref.ID).Update(db.Ctx, []firestore.Update{
				{
					Path:  "ActualCalls",
					Value: 0,
				},
			})
			// make the data to be posted
			marshaledData, _ := json.Marshal(utilities.WebHookStruct2{
				WebhookId: doc.Ref.ID,
				Country:   doc.Data()["Country"].(string),
				Calls:     int(doc.Data()["Calls"].(int64)),
			})

			// post the data
			_, err := http.Post(doc.Data()["Url"].(string), "application/json", bytes.NewBuffer(marshaledData))
			if err != nil {
				http.Error(w, "Internal server error", http.StatusInternalServerError)
				fmt.Println(err.Error())
				break
			}
		}

		if err != nil {
			break
		}
	}

}
