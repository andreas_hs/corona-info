package policyHandlers

import (
	"encoding/json"
	"google.golang.org/api/iterator"
	"io"
	"io/ioutil"
	"main/db"
	"main/handlers/cases_handlers"
	"main/handlers/notificationHandlers"
	"main/utilities"
	"net/http"
	"regexp"
	"strings"
	"time"
)

func PolicyHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		handlePolicyGetRequest(w, r)
	default:
		http.Error(w, "method not supported", http.StatusMethodNotAllowed)
	}
}

func handlePolicyGetRequest(w http.ResponseWriter, r *http.Request) {
	name, date := getNameAndDate(w, r)
	if name == "" || date == "" {
		return
	}
	resp := getResponse(w, r, name, date)
	replyData(w, resp)
}

// gets the name and date from the url
func getNameAndDate(w http.ResponseWriter, r *http.Request) (string, string) {
	// sets date to today's date using a specific format
	date := time.Now().Format("2006-01-02")
	// splits the url.path on every /
	parts := strings.Split(r.URL.Path, "/")

	// checks if there are to many arguments
	if len(parts) > 5 {
		http.Error(w, "Too many arguments", http.StatusBadRequest) // status bad request
		return "", ""
	}
	// sets name to the 4th part
	name := strings.ToUpper(parts[4])
	// checks if name is empty
	if name == "" {
		http.Error(w, "Bad gateway", http.StatusBadGateway)
	}
	if r.URL.RawQuery != "" {
		// if date is given it is set to that instead of today's date
		rawQueryParts := strings.Split(r.URL.RawQuery, "=")
		date = rawQueryParts[1]
		if !validateDate(w, date) {
			date = ""
		}
	}
	//returns name and date
	return name, date
}

func validateDate(w http.ResponseWriter, date string) bool {
	regex := regexp.MustCompile("^(2019|202\\d)-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$")
	// day - (0?[1-9]|[12][0-9]|3[01])
	// month - (0?[1-9]|1[012])
	// year - ((19|20)\d\d)
	if !regex.MatchString(date) { // If wrong format it will enter this if
		http.Error(w, "Bad request, double check scope input needs to be in format YYYY-MM-DD.", http.StatusBadRequest)
		return false
	}
	return true
}

func getResponse(w http.ResponseWriter, r *http.Request, name string, date string) utilities.Response {

	// builds the string that is used in the get request
	var stringToSend strings.Builder
	stringToSend.WriteString(utilities.PolicyApi + utilities.PolicyActionsEndpoint)
	stringToSend.WriteString(name)
	stringToSend.WriteString("/")
	stringToSend.WriteString(date)

	//check cache
	cacheResp := checkPolicyCache(name, date)

	if (cacheResp != utilities.Response{}) {
		return cacheResp
	}
	// sends get request
	resp, err := http.Get(stringToSend.String())
	// error handling
	if resp == nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return utilities.Response{}
	}
	if err != nil {
		http.Error(w, "Bad Gateway", http.StatusBadGateway)
		return utilities.Response{}
	}

	// closes the body when method is finished
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return utilities.Response{}
	}

	var response utilities.PolicyActions
	// unmarshal the body into the struct
	err = json.Unmarshal(body, &response)
	// error handling
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError) // fix here

		return utilities.Response{}
	}
	// returns the response from the api
	var nrOfPolicies = 0
	for range response.Policies {
		nrOfPolicies++
	}
	// if the policy type code is "NONE" nr of policies is set to 0
	if response.Policies[0].Policy_type_code == "NONE" {
		nrOfPolicies--
	}

	var realStringency = 0.0
	if response.Stringency.StringencyActual > 0.0 {
		realStringency = response.Stringency.StringencyActual
	} else if response.Stringency.Stringency > 0.0 {
		realStringency = response.Stringency.Stringency
	} else {
		realStringency = -1
	}

	newResp := utilities.Response{
		CountryCode: name,
		Scope:       date,
		Stringency:  realStringency,
		Policies:    nrOfPolicies,
	}
	a := strings.Title(strings.ToLower(cases_handlers.FindCountryUsingAlpha(w, name)))
	go notificationHandlers.CountUses(w, a)

	id, _, err := db.Client.Collection(utilities.PolicyCollection).Add(db.Ctx, newResp)
	if err != nil {
		// Error handling
		http.Error(w, "Error when adding "+id.ID+", Error: "+err.Error(), http.StatusBadRequest)
		return newResp
	}
	return newResp
}

func replyData(w http.ResponseWriter, response utilities.Response) {

	if response == (utilities.Response{}) {
		return
	}
	w.Header().Add("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")
	err := encoder.Encode(response)
	if err != nil {
		http.Error(w, "ERROR encoding.", http.StatusInternalServerError)
		return
	}
}

func checkPolicyCache(countryName string, scope string) utilities.Response {
	iter := db.Client.Collection(utilities.PolicyCollection).Where("countryCode", "==", countryName).Where("scope", "==", scope).Documents(db.Ctx)
	var response utilities.Response
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			break
		}

		response = utilities.Response{
			CountryCode: doc.Data()["countryCode"].(string),
			Scope:       doc.Data()["scope"].(string),
			Stringency:  doc.Data()["stringency"].(float64),
			Policies:    int(doc.Data()["policies"].(int64)),
		}
		return response
	}
	return utilities.Response{}
}
