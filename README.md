# assignment-3

### author: Andreas Sellesbakk


# ASSIGNMENT 3 - CORONA INFO
[TOC]

## Description
REST web application in Golang that provides the client with the ability to retrieve information about Corona cases
occurring in different countries, as well as the number and stringency of current policies in place.


The REST web services used for this purpose is:
* Covid 19 Cases API: https://github.com/rlindskog/covid19-graphql
* Corona Policy Stringency API: https://covidtracker.bsg.ox.ac.uk/about-api
* Country API: https://restcountries.com/
    * Documentation/Source under: https://gitlab.com/amatos/rest-countries

## Endpoints
### Overview
```
/
/corona/v1/cases/
/corona/v1/policy/
/corona/v1/status/
/corona/v1/notifications/
```
Running on the server, the endpoints will be following:
```
http://10.212.139.58:8080/corona/v1/cases/
http://10.212.139.58:8080/corona/v1/policy/
http://10.212.139.58:8080/corona/v1/status/
http://10.212.139.58:8080/corona/v1/notifications/
```

### Covid-19 Cases per Country
#### Request
```
Method: GET
Path: /corona/v1/cases/{:country_name}
```
This endpoint returns the latest number of confirmed cases and deaths for a given country, alongside growth rate of cases.
```{:country_name}``` refers to the name of the country as supported by the *Covid 19 cases API*. This web service also allows the
ISO 3166-1 alpha-3 country code as input.

#### Response
* Content type: ```application/json```
* Body(example with {:country_name}= "Norway"):
```json
{
    "country": "Norway",
    "date": "2022-03-05",
    "confirmed": 1305006,
    "recovered": 0,
    "deaths": 1664,
    "growth_rate": 0.004199149089414866
}
```

### Covid Policy Stringency per Country
#### Request
```
Method: GET
Path: /corona/v1/policy/{:country_code}{?scope=YYYY-MM-DD}
```
This endpoint  provides an overview of the *current stringency level* of policies regarding Covid-19 for a given country,
in addition to the number of currently active policies.
* ```{:country_code}```  refers to the ISO 3166-1 alpha-3 country code.
* ```{?scope=YYYY-MM-DD}``` indicates the date for which the policy stringency information should be returned. Note that
  this field is optional and has to be formatted as specified. If no date is provided, the current date will be used.

####Response
* Content type: ```application/json```
  Body (example with {:country_code} = FRA and {?scope=YYYY-MM-DD} = "2022-03-01"):
```json
{
"country_code": "FRA",
"scope": "2022-03-01",
"stringency": 63.89,
"policies": 0
}
```

If the 'stringency actual' field of the API response is 0, the 'stringency' field will be returned. If both has value 0, then
the value -1 wil be reported. If there are no policies, the value 0 will be returned.

### Status interface
#### Request
```
Method: GET
Path: /corona/v1/status/
```
This endpoint  indicates the availability of all individual services this service depends on.  The reporting occurs
based on status codes returned by the dependent services. The status interface further provides information about the
number of registered webhooks, and the uptime of the service.

#### Response
* Content type: ```application/json```
  Body:
```
{
  "cases_api": "<http status code for *Covid 19 Cases API*>",
  "policy_api": "<http status code for *Corona Policy Stringency API*>",
  "country_api: "<http status code for *Country API*>",
  "webhooks": "<number of registered webhooks>",
  "version": "v1",
  "uptime": <time from the last service restart>
  }
```
### Notification Endpoint
As an additional feature, users can register webhooks that are triggered by the service based on specified events,
specifically if information about given countries is invoked, where the minimum frequency can be specified.
Users can register multiple webhooks. The service is persistent, therefore registrations survives a service restart.
#### Registration of webhook
####- Request
```
Method: POST
Path: /corona/v1/notifications/
```
* Content type: ```application/json```
* Body contains:
    * the URL to be triggered upon event (the service that should be invoked)
    * the country for which the trigger applies
    * the minimum number of repeated invocations before notification should occur (i.e., "greater equals")
    * additionally: a count for number of times the specified country has been requested in the application
      Body (example)
```json
{
   "url": "https://localhost:8080/client/",
   "country": "France",
   "calls": 5
}
```
####- Response
The response contains the ID for the registration that can be used to see detail information or to delete the webhook
registration. The ID of a webhook is unique.
* Content type: ```application/json```
  Body (example with the example body from *request*):
```json
{
    "webhook_id": "oFnzOOyKNjbtu6c",
    "url": "https://localhost:8080/client/",
   "country": "France",
   "calls": 5
}
```

#### Deletion of Webhook
#### - Request
```
Method: DELETE
Path: /corona/v1/notifications/{id}
```
* {id} is the ID returned during the webhook registration (the id of the webhook
#### - Response
The response returns the webhook id provided if is deleted. If the webhook does not exist, an error will occur.
Body (example):
```Webhook deleted with identifier: C5jLU7GBjnCVyHn```

#### View registered webhook
#### - Request
```
Method: GET
Path: /corona/v1/notifications/{id}
```
* {id} is the ID returned during the webhook registration (the id of the webhook)

#### - Response
The response is similar to the POST request body, but further includes the ID assigned by the server upon adding
the webhook.
* Content type: ```application/json```
  Body (example where {id} = OIdksUDwveiwe):
```json
{
   "webhook_id": "OIdksUDwveiwe",
   "url": "https://localhost:8080/client/",
   "country": "France",
   "calls": 5
}
```
#### View registered webhook
#### - Request
```
Method: GET
Path: /corona/v1/notifications/
```
To view all the registered webhooks

#### - Response
The response is similar to the POST request body, but further includes the ID assigned by the server upon adding
the webhook.
* Content type: ```application/json```
  Body (example):
```json
[{
   "webhook_id": "OIdksUDwveiwe",
   "url": "https://localhost:8080/client/",
   "country": "France",
   "calls": 5
},
{
   "webhook_id": "C5jLU7GBjnCVyHn",
   "url": "https://localhost:8080/webhooks1/",
   "country": "Norway",
   "calls": 12
}
]
```
#### Webhook invocation
When a webhok is triggered, it sends the url as specified by the user when registering the webhook. Where multiple
webhooks are triggered, the urls are sent separately.
```
Method: POST
Path: <url specified in the corresponding webhook registration>
```
* Content type: ```application/json```
  Body (example):
```json
{
   "webhook_id": "OIdksUDwveiwe",
   "country": "Norway",
   "calls": 3
}
```


#### Docker deployment link:
The application is hosted using docker on google cloud platform

URL: ```http://35.198.161.189:8080/``` 

